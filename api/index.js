const { queueCreate, queueDelete, queueFindOne, queueUpdate, queueView } = require('../src/Adaptadores')


async function Create({ name, age, color }) {

    try {
        //END POINT 
        const job = await queueCreate.add({ name, age, color })


        const {statusCode, data, message} = await job.finished()

        if(statusCode === 200){
            console.log('Bienvenido ', data.name)
        }else{
            console.error(message)
        }

    } catch (error) {

        console.log(error)

    }


}

async function Delete({ id }) {

    try {
        //END POINT 
        const job = await queueDelete.add({ id })


        const {statusCode, data, message} = await job.finished()

        if(statusCode === 200) console.log('Se eliminó el usuario', data.name)
        else console.error('error')

    } catch (error) {

        console.log(error)

    }


}

async function FindONe({ id }) {

    try {
        //END POINT 
        const job = await queueFindOne.add({ id })


        const {statusCode, data, message} = await job.finished()

        if(statusCode === 200)console.log('EL usuario que buscó es: ',data)
        else console.error('error')

    } catch (error) {

        console.log(error)

    }


}

async function Update({ name, age, color, id }) {

    try {
        //END POINT 
        const job = await queueUpdate.add({ name, age, color, id })


        const {statusCode, data, message} = await job.finished()

        console.log({statusCode, data, message})

    } catch (error) {

        console.log(error)

    }


}

async function View({}) {

    try {
        //END POINT 
        const job = await queueView.add({})


        const {statusCode, data, message} = await job.finished()

        if(statusCode === 200) for(let x of data) console.log(x)
        else console.error(error)

    } catch (error) {

        console.log(error)

    }


}

async function main(){

    //await Create({name:'Daniel', age:'23', color:'Naranjini'})

    //await Delete({id:8})
    //await Update({id:1, name: 'ELMACHO'})
    //await View({})
    await FindONe({id:1})
}

main()


