//IMPORTANDO EL ADAPTADOR:
const { Adaptador } = require('./src/Adaptadores')

const main = async () => {

    try {
        const resultado = await Adaptador({id: 1})

        if(resultado.statusCode !==200) throw(resultado.message)

        else console.log('Tu data es: ', resultado.data)

    } catch (error) {
        console.log(error)
    }
}

main()


