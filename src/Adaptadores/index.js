const bull = require('bull')
const { redis } = require('../settings')


console.log('Bull endpoint ready')

const opts = { redis: { host: redis.host, port: redis.port } }

//'curso' Es un endpoint, bull será nuestro adaptador, lo usaremos para conectarnos con el exterior
//Usaremos varios endpoints
const queueCreate = bull("curso:create", opts)
const queueDelete = bull("curso:delete", opts)
const queueUpdate = bull("curso:update", opts)
const queueFindOne = bull("curso:findOne", opts)
const queueView = bull("curso:view", opts)
module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView }
