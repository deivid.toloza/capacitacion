const Services = require('../Servicios')
const { InternalError } = require('../settings')
const { queueView, queueCreate, queueDelete, queueFindOne, queueUpdate } = require('./index')


async function View(job, done) {

    try {
        const { } = job.data

        let { statusCode, data, message } = await Services.View({})

        done(null, { statusCode, data, message })
    }
    catch (error) {

        console.log({ step: 'adaptador queueView', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError })

    }

}

async function Create(job, done) {

    try {
        const { name, age, color } = job.data

        let { statusCode, data, message } = await Services.Create({ name, age, color })

        done(null, { statusCode, data, message })
    }
    catch (error) {

        console.log({ step: 'adaptador queueCreate', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError })

    }

}

async function Delete(job, done) {

    try {
        const { id } = job.data

        let { statusCode, data, message } = await Services.Delete({ id })

        done(null, { statusCode, data, message })
    }
    catch (error) {

        console.log({ step: 'adaptador queueDelete', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError })

    }

}

async function FindOne(job, done) {

    try {
        const { id } = job.data

        let { statusCode, data, message } = await Services.FindOne({ id })

        done(null, { statusCode, data, message })
    }
    catch (error) {

        console.log({ step: 'adaptador queueFindOne', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError })

    }

}

async function Update(job, done) {

    try {
        const { name, age, color, id } = job.data

        let { statusCode, data, message } = await Services.Update({ name, age, color, id })

        done(null, { statusCode, data, message })
    }
    catch (error) {

        console.log({ step: 'adaptador queueUpdate', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError })

    }

}

async function run() {
    try {

        console.log('run corriendo')

        queueView.process(View)

        queueCreate.process(Create)

        queueDelete.process(Delete)

        queueFindOne.process(FindOne)

        queueUpdate.process(Update)

    } catch (error) {

        console.log(error)

    }
}

module.exports = { View, Create, Delete, FindOne, Update, run }