const { Model } = require('../Models')

//CRUD 

async function Create({ name, age, color }) {

    try {

        let instance = await Model.create(
            { name, age, color },
            { fields: ['name', 'age', 'color'], logging: false }
        )

        return { statusCode: 200, data: instance.toJSON() }


    } catch (error) {

        console.log({ step: 'controller Create', error: error.toString() })

        return { statusCode: 400, message: error.toString() }

    }
}

async function Delete({ where = {} }) {

    try {

        let instance = await Model.destroy({ where, logging: false })

        return { statusCode: 200, data: 'OK' }

    } catch (error) {

        console.log({ step: 'controller Delete', error: error.toString() })

        return { statusCode: 400, message: error.toString() }

    }
}

async function Update({ name, age, color, id }) {

    try {

        let instance = await Model.update(
            { name, age, color },
            { where: { id }, logging: false, returning: true }
        )

        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {

        console.log({ step: 'controller Update', error: error.toString() })

        return { statusCode: 400, message: error.toString() }

    }
}

async function FindOne({ where = { id } }) {

    try {

        let instance = await Model.findOne({ where, logging: false })

        if (instance) return { statusCode: 200, data: instance.toJSON() }

        else return { statusCode: 400, message: 'NO EXISTE EL USUARIO' }

    } catch (error) {

        console.log({ step: 'controller FindOne', error: error.toString() })

        return { statusCode: 500, message: error.toString() }

    }
}

async function View({ where }) {

    try {

        let instances = await Model.findAll({ where, logging: false })

        return { statusCode: 200, data: instances }

    } catch (error) {

        console.log({ step: 'controller View', error: error.toString() })

        return { statusCode: 400, message: error.toString() }

    }
}
module.exports = { Create, Delete, Update, FindOne, View }