const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize')

//Modelo de base de datos
const Model = sequelize.define('curso', {

    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING },
})

//Sincronización con la base de datos
async function SyncDB() {

    try {

        console.log('Inicializando base de datos...')

        await Model.sync({ logging: false })

        console.log('Base de datos inicializada')

        return { statusCode: 200, data: 'ok' }

    } catch (error) {

        console.log(error)

        return { statusCode: 500, message: error.toString() }

    }
}

module.exports = { SyncDB, Model }