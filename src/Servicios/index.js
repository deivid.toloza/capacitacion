const Controladores = require('../Controladores')
const { InternalError } = require('../settings')

//Servicio llama los controladores y aquí se hace la lógica
async function Create({ name, age, color }) {

    try {

        let { statusCode, data, message } = await Controladores.Create({ name, age, color })

        return { statusCode, data, message }

    } catch (error) {

        console.log({ step: 'service Create', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }


}

async function Delete({ id }) {

    try {

        const findONe = await Controladores.FindOne({ where: { id } })

        if (findONe.statusCode !== 200) {

            let response = {
                400: { statusCode: 400, message: 'No existe el usuario a eliminar' },
                500: { statusCode: 500, message: InternalError }
            }

            return response[findONe.statusCode]

            //AQUI PODRÍA USAR UN SWITCH EN VEZ DE ESTO, ASÍ:
            /* switch (findONe.statusCode) {
                case 400:
                    return{ statusCode: 400, message: 'No existe el usuario a eliminar' }
                case 500:
                    return { statusCode: 500, message: InternalError }

                default:
                    return {statusCode: findONe.statusCode, message: findONe.message}
            } */
        }

        const del = await Controladores.Delete({ where: { id } })

        if (del.statusCode === 200) return { statusCode: 200, data: findONe.data }

        return {statusCode: 400, message: InternalError}

    } catch (error) {

        console.log({ step: 'service Delete', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }


}
async function Update({ name, age, color, id }) {

    try {

        let { statusCode, data, message } = await Controladores.Update({ name, age, color, id })

        return { statusCode, data, message }

    } catch (error) {

        console.log({ step: 'service Update', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }


}
async function FindOne({ id }) {

    try {

        let { statusCode, data, message } = await Controladores.FindOne({ where: { id } })

        return { statusCode, data, message }

    } catch (error) {

        console.log({ step: 'service FindONe', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }


}
async function View({ }) {

    try {

        let { statusCode, data, message } = await Controladores.View({})

        return { statusCode, data, message }

    } catch (error) {

        console.log({ step: 'service View', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }


}

module.exports = { Create, Delete, Update, FindOne, View }