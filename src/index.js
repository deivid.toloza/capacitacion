const { SyncDB } = require('./Models')
const { run } = require('./Adaptadores/processor')

module.exports = { SyncDB, run }